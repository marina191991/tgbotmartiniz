# Telegram Bot

Это Telegram-бот, созданный с использованием фреймворка Laravel. Бот прослушивает обновления от пользователей Telegram с помощью "Long Polling" и реагирует на определенные команды.

## Установка

1. Клонируйте репозиторий на свой локальный компьютер.
2. Запустите команду `composer install`, чтобы установить зависимости проекта.
3. Переименуйте файл `.env.example` в `.env` и настройте необходимую конфигурацию.

## Как использовать

1. Зарегистрируйте своего бота на платформе Telegram и получите токен доступа.
2. В файле `.env` установите значение переменной `TELEGRAM_BOT_TOKEN` равным вашему токену доступа.

## Описание кода

### Класс `TelegramController`

Этот класс является контроллером, отвечающим за прослушивание и обработку обновлений от Telegram.
В методе `listen` осуществляется бесконечный цикл прослушивания, в котором получаются обновления от Telegram 
и обрабатываются соответствующим образом. За отправку сообщений используется экземпляр класса `Telegram`.

### Класс `Telegram`

Этот класс представляет собой хелпер-класс для взаимодействия с Telegram API. Он содержит методы для отправки различных 
типов сообщений, таких как текстовые сообщения, сообщения с кнопками и т.д. Класс также хранит URL-адрес API Telegram 
и токен бота.

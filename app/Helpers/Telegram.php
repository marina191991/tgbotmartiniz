<?php

namespace App\Helpers;

use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;

class Telegram
{
    const url = 'https://api.telegram.org/bot';
    protected Http $http;
    protected string $bot;

    public function __construct(Http $http, string $bot)
    {
        $this->http = $http;
        $this->bot = $bot;
    }

    public function sendMessage($chat_id, string $message): Response
    {
        return $this->http::post(
            self::url . $this->bot . "/sendMessage",
            [
                'chat_id' => (string)$chat_id,
                'text' => $message,
                'parse_mode' => 'html'
            ]
        );
    }

    public function sendMessageInlineKeyboard(
        $chat_id,
        string $titleButton,
        string $webApp
    ): Response {

        $button = [
            'text' => 'Open Web App',
            'web_app' => ['url' => $webApp],
        ];

        $keyboard = [
            [$button]
        ];

        return
            $this->http::post(
                self::url . $this->bot . "/sendMessage",
                [
                    'chat_id' => (string)$chat_id,
                    'text' => $titleButton,
                    'reply_markup' => ['inline_keyboard' => $keyboard]
                ]
            );
    }

    public function sendPhoto($chat_id, $file): Response
    {
        return $this->http::attach('photo', file_get_contents(public_path("$file")), 'cats')
            ->post(
                self::url . $this->bot . "/sendPhoto",
                [
                    'chat_id' => (string)$chat_id,
                ]
            );
    }

    public function sendDocument($chat_id, $file): Response
    {
        return $this->http::attach('document', file_get_contents(public_path($file)), 'cats')
            ->post(
                self::url . $this->bot . "/sendDocument",
                [
                    'chat_id' => (string)$chat_id,
                ]
            );
    }

    public function getBot(): string
    {
        return $this->bot;
    }

}

<?php

namespace App\Http\Controllers;

use App\Helpers\Telegram;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class TelegramController extends Controller
{

    public function listen(Telegram $telegram)
    {
        $lastUpdateId = 0;

        while (true) {
            $response = Http::get($telegram::url . $telegram->getBot() . "/getUpdates", [
                'offset' => $lastUpdateId + 1,
            ]);

            if ($response->successful()) {
                $updates = $response->json('result');

                foreach ($updates as $update) {
                    $chatId = $update['message']['chat']['id'];
                    $userName = $update['message']['chat']['first_name'];

                    if (isset($update['message']['text']) && preg_match('/\/start .*/i', $update['message']['text'])) {
                        if (strtolower($update['message']['text']) == '/start rostov') {
                            $telegram
                                ->sendMessage($chatId, "<i>Hello, $userName. Ты из города Ростов</i>");

                            $telegram
                                ->sendMessageInlineKeyboard(
                                    $chatId,
                                    'rostov',
                                    'https://marina191991.github.io/rostovphoto/'
                                );

                            Log::info("send inline keyboard. Chat_id = $chatId");
                        }

                        if (strtolower($update['message']['text']) == '/start moscow') {
                            $telegram->sendMessage($chatId, "<i>Hello, $userName. Ты из города Москва</i>");

                            $telegram
                                ->sendMessageInlineKeyboard(
                                    $chatId,
                                    'moscow',
                                    'https://marina191991.github.io/martiniz191991.github.io/'
                                );

                            Log::info("send inline keyboard. Chat_id = $chatId");
                        }

                    } else {
                        if (isset($update['message']['text'])) {
                            $telegram
                                ->sendMessage($chatId, "<i>You wrote {$update['message']['text']}</i>");

                            Log::info("send echo. Chat_id = $chatId");

                        } else {
                            $telegram
                                ->sendMessage($chatId, "<i>What's it?</i>");

                            Log::info("Message type isn't text. Chat_id = $chatId");
                        }
                    }
                    $lastUpdateId = $update['update_id'];
                }
            }
        }
    }
}
